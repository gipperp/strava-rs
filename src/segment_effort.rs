extern crate serde;
extern crate serde_json;

use super::StravaObject;
use super::segment;

#[derive(Serialize, Deserialize)]
pub struct DetailedSegmentEffort {
    id: u128,
    pub elapsed_time: u128,
    pub moving_time: u128,
    pub segment: segment::SummarySegment,
}

impl StravaObject for DetailedSegmentEffort {
    fn get_id(&self) -> u128 {
        self.id
    }
}
