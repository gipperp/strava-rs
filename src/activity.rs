extern crate serde;
extern crate serde_json;

use super::StravaObject;
use super::segment_effort;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum ActivityType {
    AlpineSki,
    BackcountrySki,
    Canoeing,
    Crossfit,
    EBikeRide,
    Elliptical,
    Hike,
    IceSkate,
    InlineSkate,
    Kayaking,
    Kitesurf,
    NordicSki,
    Ride,
    RockClimbing,
    RollerSki,
    Rowing,
    Run,
    Snowboard,
    Snowshoe,
    StairStepper,
    StandUpPaddling,
    Surfing,
    Swim,
    VirtualRide,
    VirtualRun,
    Walk,
    WeightTraining,
    Windsurf,
    Workout,
    Yoga
}

#[derive(Serialize, Deserialize)]
pub struct DetailedActivity {
    id: u128,
    pub name: String,
    #[serde(rename="type")]
    pub _type: ActivityType,
    pub device_name: String,
    pub distance: u128,
    pub moving_time: u128,
    pub elapsed_time: u128,
    pub total_elevation_gain: u128,
    pub segment_efforts: Vec<segment_effort::DetailedSegmentEffort>,
}

impl DetailedActivity {
    pub fn new(j: &str) -> DetailedActivity {
        serde_json::from_str(j).unwrap()
    }
}

impl StravaObject for DetailedActivity {
    fn get_id(&self) -> u128 {
        self.id
    }
}
