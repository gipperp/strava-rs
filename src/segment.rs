extern crate serde;
extern crate serde_json;

use super::StravaObject;

#[derive(Serialize, Deserialize)]
pub struct SummarySegment {
    id: u128,
    pub name: String,
    pub distance: f64,
    pub average_grade: f64,
    pub maximum_grade: f64,
}

impl StravaObject for SummarySegment {
    fn get_id(&self) -> u128 {
        self.id
    }
}
