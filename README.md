# strava-rs
This is intended to be a [Rust](https://rust-lang.org) library for interacting with the [Strava API](https://developers.strava.com/).
It is very much a work-in-progress, and is being used as a learning tool for me with Rust.
